<%-- 
    Document   : form_inserir_usuario
    Author     : André
--%>

<%@page import="modelo.UsuarioDAO"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="modelo.Perfil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modelo.PerfilDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formulário de Alteração - Usuário</title>
        <link href="css/custom-theme/jquery-ui-1.8.21.custom.css" rel="stylesheet" type="text/css">
        <link href="css/main.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="js/ajax.js"></script>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
        <script type="text/javascript" src="js/querySets.js"></script>
        <script type="text/javascript" src="js/canvasManager.js"></script>
        <script type="text/javascript">
            function validaForm(){

            var form_alterar_usuario=document.form_alterar_usuario;
                var campo_nome=form_alterar_usuario.nome;
                var campo_id_perfil=form_alterar_usuario.id_perfil;
                var campo_login=form_alterar_usuario.login;
                var campo_senha=form_alterar_usuario.senha;

                if(campo_nome.value==""){
                    alert("Todos os campos devem ser preenchidos!");
                    campo_nome.focus();
                    return false;
                }
                if(campo_id_perfil.value=="0"){
                    alert("Todos os campos devem ser preenchidos!");
                    campo_id_perfil.focus();
                    return false;
                }
                if(campo_login.value==""){
                    alert("Todos os campos devem ser preenchidos!");
                    campo_login.focus();
                    return false;
                }
                if(campo_senha.value==""){
                    alert("Todos os campos devem ser preenchidos!");
                    campo_senha.focus();
                    return false;
                }

                return true;
        }
        </script>
    </head>
    <body>
        <div class="selfcontainer" align="center">
        <div class="header">
            <%@include file="menu.jsp" %>
        </div>
                    <div class="content">
                    <table class="filled tableMin">
                <tr>
                    <td class="filled" valign="top">
                        <table  align="center" >
                            <tr>
                                <td align="left" ><h1>Alterar Usuário</h1></td>
                            </tr>
                        </table>
                        <table align="center" width="500">
                                                            <%

                                        try {
                                            int id = Integer.parseInt(request.getParameter("id"));
                                            ArrayList<Perfil> lista1 = new ArrayList<Perfil>();

                                            UsuarioDAO uDB = new UsuarioDAO();
                                            uDB.conectar();
                                            Usuario u = uDB.carregaPorId(id);
                                            uDB.desconectar();

 
                                                PerfilDAO pDB = new PerfilDAO();
                                                pDB.conectar();
                                                ArrayList<Perfil> lista = pDB.listar();

                                %>
                                <form name="form_alterar_usuario" action="alterar_usuario.do" method="POST" onsubmit="return validaForm()">
                                    <tr>
                                    <td>Id:</td>
                                    <td><input type="text" readonly size="45" name="id" value="<%=u.getId()%>"/> </td>
                                </tr>
                                <tr>
                                    <td>Nome:</td>
                                    <td><input type="text" size="45" name="nome" value="<%=u.getNome()%>"/> </td>
                                </tr>
                                <tr>
                                    <td>
                                        Perfil:
                                    </td>
                                    <td><select name="id_perfil" size="1">
                                            
                                            <%for (Perfil p : lista) {%>
                                            <%if(u.getPerfil().getId() == p.getId()) {%>
                                            <option value="<%=p.getId()%>">
                                                <%=p.getPerfil()%>
                                            </option>
                                            <%}else {lista1.add(p);}
                                            }
                                            for (Perfil p1 : lista1) {%>
                                            <option value="<%=p1.getId()%>">
                                                <%=p1.getPerfil()%>
                                            </option>
                                            <%}%>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Login:</td>
                                    <td><input type="text" size="45" name="login" value="<%=u.getLogin()%>"/> </td>
                                </tr>
                                <tr>
                                    <td>Senha:</td>
                                    <td><input type="password" size="45" name="senha" value=""/> </td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td><input class="button" type="submit" value="Alterar"/> </td>
                                </tr>
                            </form>
                        </table>
                        <%
                                        pDB.desconectar();
                                    } catch (Exception e) {
                                        out.println(e);

                                    }
                        %>
                    </td>
                </tr>
            </table>
        </div>
                    <div class="footer">
            </div>
       </div>
<%

    if(logged){
    if(session.getAttribute("usuario") == null){
       response.sendRedirect("index.jsp?erro=1");
    }
    }

%>
    </body>
</html>
